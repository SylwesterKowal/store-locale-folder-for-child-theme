<?php


namespace M21\StoreLocaleFolderForChildTheme\Observer\Controller;

class ActionPredispatch implements \Magento\Framework\Event\ObserverInterface
{

    protected $_dir;
    protected $_store;
    protected $_desig;
    protected $themeProvider;


    public function __construct(

        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Framework\View\DesignInterface $desig,
        \Magento\Framework\View\Design\Theme\ThemeProviderInterface $themeProvider

    )
    {

        $this->_dir = $dir;
        $this->_store = $store;
        $this->_desig = $desig;
        $this->themeProvider = $themeProvider;

    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        $static = $this->_dir->getPath('static'); // odczytujemy sciezke do folderu "static"
        $locale = $this->_store->getLocale(); // odczytujemy kod "locale" np. pl_PL

        $theme = $this->_desig->getDesignTheme(); // dane domyślnego szablonu child
        $curent_theme = $theme->getData();

        $parent_theme = $this->themeProvider->getThemeById($curent_theme['parent_id']); // dane szablonu nadrzedneigo dla którego magento nie generuje scierzki do folderu css

        $path = $static . '/frontend/' . $parent_theme['theme_path'] . '/' . $locale . '/css';

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    }
}
