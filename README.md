# pub/static/frontend/en_US/css

Instalując Child Theme dla szablonu Magento 2 przy każdym poleceniu upgrade trzeba dodawać folder pub/static/frontend/en_US/css
Sytuacja staje się dość upierdliwa gdy w sklepie instalujemy np. 20 języków i wówczas operacja zakładania folderu musi być wykonana 20 razy.

Jeśli ktoś jeszcze ma podobny problem to dobrym rozwiązaneim jest moduł, który automatycznie uzupełnia brakujące foldery.

EN:

When installing Child Theme for the Magento 2 template, you must add the pub / static / frontend / en_US / css folder with each upgrade command.
The situation becomes quite annoying when we install, for example, 20 languages in the store and then the operation of setting up the folder must be done 20 times.

If someone else has a similar problem then a good solution is a module that automatically completes the missing folders.


#instalacja
`composer require m21/module-storelocalefolderforchildtheme`

#moduły magento

https://magento.21w.pl 

tel.: +48 608 012 047